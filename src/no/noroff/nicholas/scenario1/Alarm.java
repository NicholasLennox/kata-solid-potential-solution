package no.noroff.nicholas.scenario1;

/*
 Alarm is a high level class and it is depending on a low level class - sensor.
 Dependency inversion says that this should not happen and they should both depend in abstractions.
*/

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    /*
     What if we want to use a new type of sensor? We have to change alarm.
     The open/closed principle says that classes should be open for extension but closed for modification.
     This means the sensor behaviour should be able to be changed without changing alarm,
     but by simply adding new code somewhere else.
    */
    Sensor sensor = new Sensor();
    boolean alarmOn = false;

    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
