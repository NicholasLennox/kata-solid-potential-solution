package no.noroff.nicholas.solution1;

// We added an interface to help solve both our issues.
// This way our high level class depends on abstraction, and we are free to change what sensor we want.
public interface Sensor {
    double popNextPressurePsiValue();
}
